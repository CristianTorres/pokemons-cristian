package co.com.cristianproyecto.pokemons.service;

import co.com.cristianproyecto.pokemons.adapter.PokemonClient;
import co.com.cristianproyecto.pokemons.commons.ChainDto;
import co.com.cristianproyecto.pokemons.commons.EvolvesDto;
import co.com.cristianproyecto.pokemons.commons.ModeloConsultaDto;
import co.com.cristianproyecto.pokemons.commons.PokemonDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

@Service
public class PokemonService {

    @Autowired
    private PokemonClient pokemonClient;

    @Autowired
    private ModelMapper modelMapper;

    public ModeloConsultaDto consultarPokemons(String offset) {
        return pokemonClient.consultarPokemons(offset);
    }

    public Map<Object, Object> consultarDetallePokemon(String pokemonId) {
        return pokemonClient.consultarDetallePokemon(pokemonId);
    }

    public PokemonDto consultarCadenaEvolucion(String pokemonId) {
        Map<Object, Object> data = pokemonClient.consultarCadenaEvolucion(pokemonId);
        ChainDto chainDto = modelMapper.map(data.get("chain"), ChainDto.class);
        String evolucion = chainDto.getSpecies().getName();
        if (!chainDto.getEvolves_to().isEmpty()) {
            evolucion = metodoRecursivo(evolucion, chainDto);
        }
        return PokemonDto.builder().chainEvolve(evolucion).build();
    }

    public String metodoRecursivo(String nombre, ChainDto cadena) {

        for (EvolvesDto evolucion : cadena.getEvolves_to()) {
            nombre = nombre.concat(" -> ").concat(evolucion.getSpecies().getName());
            if (evolucion.getEvolves_to().isEmpty()) {
                return nombre;
            } else {
                for (ChainDto chainDto : evolucion.getEvolves_to()) {
                    nombre = nombre.concat(" -> ").concat(chainDto.getSpecies().getName());
                    metodoRecursivo(nombre, chainDto);
                }
            }
        }
        return nombre;
    }

}
