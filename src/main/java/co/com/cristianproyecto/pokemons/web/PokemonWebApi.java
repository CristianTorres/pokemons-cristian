package co.com.cristianproyecto.pokemons.web;

import co.com.cristianproyecto.pokemons.commons.ModeloConsultaDto;
import co.com.cristianproyecto.pokemons.commons.PokemonDto;
import co.com.cristianproyecto.pokemons.service.PokemonService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/pokemon")
@Log4j2
@CrossOrigin(origins = "*", methods = {RequestMethod.GET})
public class PokemonWebApi {

    @Autowired
    private PokemonService pokemonService;

    @GetMapping()
    public ResponseEntity<ModeloConsultaDto> consultarPokemons(@RequestParam(defaultValue = "0", name = "offset") String offset) {
        log.trace("PokemonWebApi :: consultarPokemons :: consultando lista de pokemons");
        return ResponseEntity.ok(pokemonService.consultarPokemons(offset));
    }

    @GetMapping("/{pokemonId}")
    public ResponseEntity<Map<Object, Object>> consultarDetallePokemon(@PathVariable("pokemonId") String pokemonId) {
        log.trace("PokemonWebApi :: consultarDetallePokemon :: consultando detalle del pokemon");
        return ResponseEntity.ok(pokemonService.consultarDetallePokemon(pokemonId));
    }

    @GetMapping("/{pokemonId}/evolution")
    public ResponseEntity<PokemonDto> consultarCadenaEvolucion(@PathVariable("pokemonId") String pokemonId) {
        log.trace("PokemonWebApi :: consultarCadenaEvolucion :: consultando la cadena de evolucion del pokemon");
        return ResponseEntity.ok(pokemonService.consultarCadenaEvolucion(pokemonId));
    }

}

