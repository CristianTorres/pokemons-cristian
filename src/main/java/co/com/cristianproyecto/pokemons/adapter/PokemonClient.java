package co.com.cristianproyecto.pokemons.adapter;

import co.com.cristianproyecto.pokemons.commons.ModeloConsultaDto;
import lombok.extern.log4j.Log4j2;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Component
@Log4j2
public class PokemonClient {

    public ModeloConsultaDto consultarPokemons(String offset) {
        log.info("PokemonClient :: consultarPokemons :: consumiendo servicio de consulta de pokemons");
        String url = "https://pokeapi.co/api/v2/pokemon-species?offset=" + offset + "&limit=20";
        return generateRestemplate().getForObject(url, ModeloConsultaDto.class);
    }

    public Map<Object, Object> consultarDetallePokemon(String pokemonId) {
        log.info("PokemonClient :: consultarDetallePokemon :: consumiendo servicio de consulta de detalles");
        String url = "https://pokeapi.co/api/v2/pokemon-species/" + pokemonId;
        return generateRestemplate().getForObject(url, Map.class);
    }

    public Map<Object, Object> consultarCadenaEvolucion(String pokemonId) {
        log.info("PokemonClient :: consultarCadenaEvolucion :: consumiendo servicio de consulta de cadena de evolucion");
        String url = "https://pokeapi.co/api/v2/evolution-chain/" + pokemonId;
        return generateRestemplate().getForObject(url, Map.class);
    }

    private RestTemplate generateRestemplate() {
        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLHostnameVerifier(new NoopHostnameVerifier())
                .build();
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        return new RestTemplate(requestFactory);
    }


}
