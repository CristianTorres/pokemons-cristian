package co.com.cristianproyecto.pokemons.commons;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ResultDto {

    private String name;
    private String url;

}
