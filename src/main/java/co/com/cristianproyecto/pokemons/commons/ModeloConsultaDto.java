package co.com.cristianproyecto.pokemons.commons;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ModeloConsultaDto {

    private String count;
    private String next;
    private String previous;
    private List<ResultDto> results;

}
