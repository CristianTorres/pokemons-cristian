package co.com.cristianproyecto.pokemons.commons;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class PokemonDto {

    private String chainEvolve;

}
