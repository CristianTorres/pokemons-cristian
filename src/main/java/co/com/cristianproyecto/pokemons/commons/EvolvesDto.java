package co.com.cristianproyecto.pokemons.commons;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class EvolvesDto {

    private List<ChainDto> evolves_to;
    private SpeciesDto species;

}
