package co.com.cristianproyecto.pokemons.commons;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ChainDto {

    private List<EvolvesDto> evolves_to;
    private SpeciesDto species;

}
