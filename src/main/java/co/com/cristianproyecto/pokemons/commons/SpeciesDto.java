package co.com.cristianproyecto.pokemons.commons;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class SpeciesDto {

    private String name;
    private String url;

}
