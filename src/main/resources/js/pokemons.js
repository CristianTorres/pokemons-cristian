var page = 0;
var pageMax = 0;

const consultarPokemons = async (num) => {
    const $cuerpoTabla = document.querySelector("#cuerpoTabla");

    $cuerpoTabla.innerHTML = "";

    const respuesta = await fetch(`https://morning-citadel-64246.herokuapp.com/pokemon?offset=${num}`);
    const data = await respuesta.json();

    pageMax = data.count;
    data.results.forEach(registro => {
        const $tr = document.createElement("tr");

        let $tdNombre = document.createElement("td");
        $tdNombre.textContent = registro.name;
        $tr.appendChild($tdNombre);

        let $tdDetalle = document.createElement("td");
        $tdDetalle.innerHTML = `<button onclick="verDetalle(${extraerIdUrl(registro.url)})">Detalle</button>`;
        $tr.appendChild($tdDetalle);

        let $tdCadena = document.createElement("td");
        $tdCadena.innerHTML = `<button onclick="verCadenaEvolucion(${extraerIdUrl(registro.url)})">Evolución</button>`;
        $tr.appendChild($tdCadena);

        $cuerpoTabla.appendChild($tr);

    });

    if (page === 0) {
        page = 20;
    }
}

function siguiente() {

    page = page + 20;
    habilitarDeshabilitarBotonesPaginado();
    consultarPokemons(page);

}

function atras() {
    page = page - 20;
    habilitarDeshabilitarBotonesPaginado();
    consultarPokemons(page);
}


function habilitarDeshabilitarBotonesPaginado() {

    var buttonAtras = document.getElementById("bttAtras");
    buttonAtras.disabled = page === 0;

    var buttonSiguiente = document.getElementById("bttSiguiente");
    buttonSiguiente.disabled = page >= pageMax;
}

function extraerIdUrl(url) {
    var value = url.replace("https://pokeapi.co/api/v2/pokemon-species/", "");
    return value.replace("/", "");
}

const verDetalle = async (num) => {

    const respuesta = await fetch(`https://morning-citadel-64246.herokuapp.com/pokemon/${num}`);
    const data = await respuesta.json();
    let mensaje = `felicidad basica: ${data.base_happiness}, tasa de captura: ${data.capture_rate}, color: ${data.color.name}`;
    alert(mensaje);

}

const obtenerIdCadena = (num) => {

    const respuesta = fetch(`https://morning-citadel-64246.herokuapp.com/pokemon/${num}`,{mode: "no-cors"});
    const data = respuesta.json();
    return extraerIdUrl(data.evolution_chain.url);

}

const verCadenaEvolucion = async (num) => {

    let idPokemon = obtenerIdCadena(num);
    const respuesta = await fetch(`https://morning-citadel-64246.herokuapp.com/pokemon/${idPokemon}/evolution`,{mode: "no-cors"});
    const data = await respuesta.json();
    alert(data.chainEvolve);
}

